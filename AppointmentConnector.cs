﻿using AppointmentModule.Factories;
using AppointmentModule.Models;
using AppointmentModule.Services;
using OntologyAppDBConnector;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using OntoMsg_Module.Models;
using OntoWebCore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule
{
    public class AppointmentConnector : AppController
    {
        private clsLocalConfig localConfig;
        public clsLocalConfig LocalConfig
        {
            get { return localConfig; }
        }

        private ServiceAgentElastic serviceAgentElastic;
        private AppointmentFactory factory;
        private object connectorLocker = new object();

        private AppointmentConnectorResult appointmentResult;
        public AppointmentConnectorResult AppointmentResult
        {
            get
            {
                lock(connectorLocker)
                {
                    return appointmentResult;
                }
                
            }
            set
            {
                lock(connectorLocker)
                {
                    appointmentResult = value;
                }

            }
        }

        public async Task<AppointmentDataSourceResult> GetDataSource(string actionUrlRead, string actionUrlCreate, string actionUrlUpdate, string actionUrlDestroy)
        {
            var taskResult = await Task.Run<AppointmentDataSourceResult>(() =>
            {
                var result = new AppointmentDataSourceResult
                {
                    Result = localConfig.Globals.LState_Success.Clone(),
                    DataSource = factory.GetSchedulerDataSource(actionUrlRead, actionUrlCreate, actionUrlUpdate, actionUrlDestroy)
                };

                return result;
            });

            return taskResult;
        }

        public async Task<clsOntologyItem> SaveAppointment(AppointmentScheduler schedulerEvent, clsOntologyItem userItem)
        {
            var taskResult = await Task.Run<clsOntologyItem>(() =>
            {
                var result = serviceAgentElastic.SaveEvent(schedulerEvent, userItem);
                return result;
            });

            return taskResult;
        }

        public async Task<AppointmentConnectorResult> GetAppointments(clsOntologyItem userItem)
        {
            var taskResult = await Task.Run<AppointmentConnectorResult>(async () =>
            {
                var result = new AppointmentConnectorResult
                {
                    Result = localConfig.Globals.LState_Success.Clone()
                };

                var appointmentFilter = new Appointment
                {
                    Users = new List<SecurityModule.Models.User>
                    {
                        new SecurityModule.Models.User
                        {
                            IdUser = userItem.GUID,
                            NameUser = userItem.Name
                        }
                    }
                };

                var resultTask = await serviceAgentElastic.GetAppointments(new List<Appointment> { appointmentFilter });

                if (resultTask.ResultAppointment.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    result.Result = localConfig.Globals.LState_Error.Clone();
                    AppointmentResult = result;
                    return result;
                }

                var factoryTask = await factory.GetAppointments(resultTask);

                result.Appointments = factoryTask;
                AppointmentResult = result;
                return result;
            });

            return taskResult;
        }

        public AppointmentConnector(Globals globals) : base(globals)
        {
            localConfig = new clsLocalConfig(globals);
            Initialize();
        }

        private void Initialize()
        {
            serviceAgentElastic = new ServiceAgentElastic(localConfig);
            factory = new AppointmentFactory(localConfig);
        }
    }

    public class AppointmentDataSourceResult
    {
        public clsOntologyItem Result { get; set; }
        public Dictionary<string, object> DataSource { get; set; }
    }

    public class AppointmentConnectorResult
    {
        public clsOntologyItem Result { get; set; }
        public List<AppointmentScheduler> Appointments { get; set; }
    }
}
