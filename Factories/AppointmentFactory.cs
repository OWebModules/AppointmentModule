﻿using AppointmentModule.Models;
using AppointmentModule.Services;
using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Models;
using PartnerModule.Connectors;
using PartnerModule.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Factories
{
    public class AppointmentFactory : NotifyPropertyChange
    {
        private object appointmentLocker = new object();
        private object writeLocker = new object();

        private SessionFile sessionFile;
        public SessionFile SessionFile
        {
            get { return sessionFile; }
            set
            {
                sessionFile = value;
            }
        }
        private clsLocalConfig localConfig;

        private PartnerConnector partnerConnector;

        private List<AppointmentScheduler> appointments;
        public List<AppointmentScheduler> Appointments
        {
            get
            {
                lock(appointmentLocker)
                {
                    return appointments;
                }
                
            }
            set
            {
                lock(appointmentLocker)
                {
                    appointments = value;
                }

                RaisePropertyChanged(nameof(Appointments));
            }
        }

        private clsOntologyItem resultWrite;
        public clsOntologyItem ResultWrite
        {
            get
            {
                lock (appointmentLocker)
                {
                    return resultWrite;
                }

            }
            set
            {
                lock (appointmentLocker)
                {
                    resultWrite = value;
                }

                RaisePropertyChanged(nameof(ResultWrite));
            }
        }

        public async Task<List<AppointmentScheduler>> GetAppointments(AppointmentResult appointmentResult)
        {
            var appointments = (from appointmentToUser in appointmentResult.AppointmentsToUsers.GroupBy(appToUser => new { IdAppointment = appToUser.ID_Object, NameAppointment = appToUser.Name_Object})
                                join startItem in appointmentResult.AppointmentAttributes.Where(attr => attr.ID_AttributeType == localConfig.OItem_attribute_start.GUID)
                                     on appointmentToUser.Key.IdAppointment equals startItem.ID_Object into startItems
                                from startItem in startItems.DefaultIfEmpty()
                                join endItem in appointmentResult.AppointmentAttributes.Where(attr => attr.ID_AttributeType == localConfig.OItem_attribute_ende.GUID)
                                     on appointmentToUser.Key.IdAppointment equals endItem.ID_Object into endItems
                                from endItem in endItems.DefaultIfEmpty()
                                join refItem in appointmentResult.AppointmentsToRefs on appointmentToUser.Key.IdAppointment equals refItem.ID_Object into refItems
                                from refItem in refItems.DefaultIfEmpty()
                                select new AppointmentScheduler
                                {
                                    IdAppointment = appointmentToUser.Key.IdAppointment,
                                    NameAppointment = appointmentToUser.Key.NameAppointment,
                                    Start = startItem != null ? startItem.Val_Date : null,
                                    End = endItem != null ? endItem.Val_Date : null,
                                    uid = Guid.NewGuid().ToString()
                                }).ToList().ToList();

            //var contractors = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_contractor.GUID).ToList();
            //var contractees = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_contractee.GUID).ToList();
            //var watchers = appointmentResult.AppointmentPartners.Where(partner => partner.ID_RelationType == localConfig.OItem_relationtype_belonging_watchers.GUID).ToList();

            //var addressesPre = appointmentResult.AppointmentAddresses.Select(addr => new clsOntologyItem
            //{
            //    GUID = addr.ID_Other,
            //    Name = addr.Name_Other,
            //    GUID_Parent = localConfig.OItem_type_address.GUID,
            //    Type = localConfig.Globals.Type_Object
            //}).ToList();

            //var addressResultTask = partnerConnector.GetAddressFull(addressesPre);
            //addressResultTask.Wait();
            //var addresses = (from addressRaw in appointmentResult.AppointmentAddresses
            //                 join addressFull in addressResultTask.Result on addressRaw.ID_Other equals addressFull.Id
            //                 select new { addressRaw, addressFull }).ToList();

            appointments.ForEach(appointment =>
            {

                var user = appointmentResult.AppointmentsToUsers.Where(usr => usr.ID_Object == appointment.IdAppointment).Select(usr => new User
                {
                    IdUser = usr.ID_Other,
                    NameUser = usr.Name_Other
                }).FirstOrDefault();
                appointment.IdUser = user.IdUser;
                appointment.NameUser = user.NameUser;

            });

            Appointments = appointments;

            return Appointments;
        }

       
        public Dictionary<string, object> GetSchedulerDataSource(string actionUrlRead, string actionUrlCreate, string actionUrlUpdate, string actionUrlDestroy)
        {
            var dictResult = new Dictionary<string, object>();

            var dictTransport = new Dictionary<string, object>();
            var dictRead = new Dictionary<string, object>();

            dictRead.Add("url", actionUrlRead);
            dictRead.Add("dataType", "json");

            dictTransport.Add("read", dictRead);

            var dictCreate = new Dictionary<string, object>();

            dictCreate.Add("url", actionUrlCreate);
            dictCreate.Add("dataType", "json");

            dictTransport.Add("create", dictCreate);

            var dictUpdate = new Dictionary<string, object>();

            dictUpdate.Add("url", actionUrlUpdate);
            dictUpdate.Add("dataType", "json");

            dictTransport.Add("update", dictUpdate);

            var dictDestroy = new Dictionary<string, object>();

            dictDestroy.Add("url", actionUrlDestroy);
            dictDestroy.Add("dataType", "json");

            dictTransport.Add("destroy", dictDestroy);

            dictResult.Add("transport", dictTransport);

            var dictSchema = new Dictionary<string, object>();
            dictResult.Add("schema", dictSchema);
            //dictSchema.Add("timezone", "Europa/Berlin");

            var dictModel = new Dictionary<string, object>();
            dictSchema.Add("model", dictModel);
            var dictFields = new Dictionary<string, object>();
            dictModel.Add("id", "taskId");
            dictModel.Add("fields", dictFields);

            var dictTask = new Dictionary<string, object>();
            dictTask.Add("from", nameof(Appointment.IdAppointment));
            dictTask.Add("type", "string");

            dictFields.Add("taskId", dictTask);

            var dictTitle = new Dictionary<string, object>();
            dictTitle.Add("from", nameof(Appointment.NameAppointment));
            dictTitle.Add("defaultValue", "No title");

            var dictValidation = new Dictionary<string, object>();
            dictValidation.Add("required", true);

            dictTitle.Add("validation", dictValidation);

            dictFields.Add("title", dictTitle);

            var dictField1 = new Dictionary<string, object>();

            dictField1.Add("type", "date");
            dictField1.Add("from", nameof(Appointment.Start));
            dictFields.Add("start", dictField1);

            var dictField2 = new Dictionary<string, object>();

            dictField2.Add("type", "date");
            dictField2.Add("from", nameof(Appointment.End));
            dictFields.Add("end", dictField2);

            //var dictField3 = new Dictionary<string, object>();
            //dictField3.Add("from", "TimeZone");
            //dictFields.Add("startTimezone", dictField3);

            //var dictField4 = new Dictionary<string, object>();
            //dictField4.Add("from", "TimeZone");
            //dictFields.Add("endTimezone", dictField4);

            var dictOwner = new Dictionary<string, object>();
            dictOwner.Add("from", nameof(User.IdUser));

            dictFields.Add("ownerId", dictOwner);

            return dictResult;
        }

        public AppointmentFactory(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            partnerConnector = new PartnerConnector(localConfig.Globals);
        }

    }

    public class AppointmentSchedulerResult
    {
        public clsOntologyItem Result { get; set; }
        public List<AppointmentScheduler> Appointments { get; set; }
    }
}
