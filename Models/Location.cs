﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Models
{
    public class Location
    {
        public string IdLocation { get; set; }
        public string NameLocation { get; set; }

        public clsOntologyItem OItemPartner { get; set; }
    }
}
