﻿using OntologyAppDBConnector.Base;
using OntologyClasses.BaseClasses;
using OntoMsg_Module.Notifications;
using OntoWebCore.Models;
using PartnerModule.Models;
using SecurityModule.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Models
{
    public class Appointment : NotifyPropertyChange
    {
        private string nameRef;
        public string NameRef
        {
            get { return nameRef; }
            set
            {
                if (nameRef == value) return;

                nameRef = value;

                RaisePropertyChanged(nameof(NameRef));

            }
        }

        private string idRef;
        public string IdRef
        {
            get { return idRef; }
            set
            {
                if (idRef == value) return;

                idRef = value;

                RaisePropertyChanged(nameof(IdRef));

            }
        }

        private string idAppointment;
		public string IdAppointment
        {
            get { return idAppointment; }
            set
            {
                if (idAppointment == value) return;

                idAppointment = value;

                RaisePropertyChanged(nameof(IdAppointment));

            }
        }

        private string nameAppointment;
		public string NameAppointment
        {
            get { return nameAppointment; }
            set
            {
                if (nameAppointment == value) return;

                nameAppointment = value;

                RaisePropertyChanged(nameof(NameAppointment));

            }
        }

        private string idAttributeStart;
		public string IdAttributeStart
        {
            get { return idAttributeStart; }
            set
            {
                if (idAttributeStart == value) return;

                idAttributeStart = value;

                RaisePropertyChanged(nameof(IdAttributeStart));

            }
        }

        private DateTime? start;
        public DateTime? Start
        {
            get { return start; }
            set
            {
                if (start == value) return;

                start = value;

                RaisePropertyChanged(nameof(Start));

            }
        }

        private string idAttributeEnd;
        public string IdAttributeEnd
        {
            get { return idAttributeEnd; }
            set
            {
                if (idAttributeEnd == value) return;

                idAttributeEnd = value;

                RaisePropertyChanged(nameof(IdAttributeEnd));

            }
        }

        private DateTime? end;
        public DateTime? End
        {
            get { return end; }
            set
            {
                if (end == value) return;

                end = value;

                RaisePropertyChanged(nameof(End));

            }
        }

        private List<Room> rooms;
        public List<Room> Rooms
        {
            get { return rooms; }
            set
            {
                if (rooms == value) return;

                rooms = value;

                RaisePropertyChanged(nameof(Rooms));

            }
        }

        private List<Partner> watchers;
        public List<Partner> Watchers
        {
            get { return watchers; }
            set
            {
                if (watchers == value) return;

                watchers = value;

                RaisePropertyChanged(nameof(Watchers));

            }
        }

        private List<User> users;
        public List<User> Users
        {
            get { return users; }
            set
            {
                if (users == value) return;

                users = value;

                RaisePropertyChanged(nameof(Users));

            }
        }

        private List<Partner> contractors;
        public List<Partner> Contractors
        {
            get { return contractors; }
            set
            {
                if (contractors == value) return;

                contractors = value;

                RaisePropertyChanged(nameof(Contractors));

            }
        }

        private List<Partner> contractees;
        public List<Partner> Contractees
        {
            get { return contractees; }
            set
            {
                if (contractees == value) return;

                contractees = value;

                RaisePropertyChanged(nameof(Contractees));

            }
        }

        private List<Address> addresses;
        public List<Address> Addresses
        {
            get { return addresses; }
            set
            {
                if (addresses == value) return;

                addresses = value;

                RaisePropertyChanged(nameof(Addresses));

            }
        }

        private List<Location> locations;
        public List<Location> Locations
        {
            get { return locations; }
            set
            {
                if (locations == value) return;

                locations = value;

                RaisePropertyChanged(nameof(Locations));

            }
        }

        public clsOntologyItem OItemAppointment { get; set; }
        public clsOntologyItem OItemRef { get; set; }
        public string UId { get; set; }

        public Appointment()
        {

        }

        public Appointment(KendoSchedulerEvent schedulerEvent, clsOntologyItem userItem)
        {
            Start = schedulerEvent.start;
            End = schedulerEvent.end;
            Users = new List<User>()
            {
                new User
                {
                    IdUser = userItem.GUID,
                    NameUser = userItem.Name
                }
            };

            IdAppointment = schedulerEvent.taskId.ToString();
            NameAppointment = schedulerEvent.title;
            UId = schedulerEvent.uid;
        }
    }
}
