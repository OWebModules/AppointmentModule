﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Models
{
    public class AppointmentScheduler
    {
        public string IdAppointment { get; set; }
        public string NameAppointment { get; set; }
	    public DateTime? Start { get; set; }
        public DateTime? End { get; set; }
        public string IdUser { get; set; }
        public string NameUser { get; set; }
        public string uid { get; set; }
    }
}
