﻿using OntologyClasses.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppointmentModule.Models
{
    public class Room
    {
        public string IdRoom { get; set; }
        public string NameRoom { get; set; }

        public clsOntologyItem OItemRoom { get; set; }
    }
}
